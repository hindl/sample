import { Box, Paper, Typography } from '@mui/material';
import { useState } from 'react';
import Form from './Form';

function App() {
  const [result, setResult] = useState<number>();

  return (
    <Box
      className="app"
      sx={{
        height: '100vh',
        width: '100vw',
        backgroundColor: 'lightcoral',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Paper sx={{ p: 4, maxWidth: '400px' }}>
        <Form onSubmit={(r) => setResult(r)} />
        {result !== null && result !== undefined && (
          <Typography sx={{ mt: 2, textAlign: 'center' }} variant="h3">
            Result: <b>{result}</b>
          </Typography>
        )}
      </Paper>
    </Box>
  );
}

export default App;
