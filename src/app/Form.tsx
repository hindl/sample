import { Button, Grid, TextField, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { useForm } from 'react-hook-form';

export interface FormData {
  inputA: string;
  inputB: string;
}

interface FormProps {
  onSubmit: (result: number) => void;
}

function Form({ onSubmit: parentOnSubmit }: FormProps) {
  const { register, handleSubmit } = useForm<FormData>();

  const onSubmit = (data: FormData) => {
    parentOnSubmit(parseFloat(data.inputA) + parseFloat(data.inputB));
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack>
        <Grid container alignItems={'center'} textAlign="center">
          <Grid item xs={5}>
            <TextField label="A" {...register('inputA')} />
          </Grid>
          <Grid item xs={2}>
            <Typography variant="h2">+</Typography>
          </Grid>
          <Grid item xs={5}>
            <TextField label="B" {...register('inputB')} />
          </Grid>
        </Grid>
        <Button variant="contained" type="submit">
          Calculate
        </Button>
      </Stack>
    </form>
  );
}

export default Form;
