# Exercice 1 - Intégration continue

Dans ce première exercice vous allez créer votre premier pipeline.
Ce dernier aura pour but d'automatiser les tests de l'application et de build notre application.

> Aidez-vous de la documentation officielle de Gitlab !

## Consigne

Le pipeline doit automatiser :

- Le formattage avec Prettier
- Le lintage avec ESLint
- Les tests unitaires avec Jest
- Les tests End-2-end avec Cypress
- Le build de l'application

> Jetez un oeuil au `package.json` !

Les tests doivent être lancés en parallèles, tandis que le build ne doit être lancé seulement si tous les tests passent.
Le résultat du build doit être téléchargeable depuis l'interface.

## Bonus

### Le cache

Améliorez le temps d'exécution moyen en mettant en cache les `node_modules`.
De cette manière il n'est pas nécessaires de réinstaller toutes les dépendances à chaque job.

> 🚀 Optimisez encore plus en utilisant le `cache.policy` !

### Les conditions

Faites en sorte que les jobs ne se lancent que depuis la branche principale et depuis une merge request.

> ℹ Certains sont des Lucky Luke du `git push`. Faire en sorte que les tests ne se fassent que lorsque c'est nécessaire réduira drastiquement la facture à la fin du mois.

### Les reports

Prettier, ESLint, Jest et Cypress exportent tous des reporting.
En vous servant de la documentation de Gitlab, faites en sorte de rendre accessible les résultats dans vos merge requests.

Les reporting de Prettier et ESLint ne sont pas conformes aux attentes de Gitlab, vous devez mettre à jour le contenu grâce à la commande suivante :

```bash
cat gl-codequality.json | jq 'map(.severity = (.severity // "minor"))' | tee gl-codequality.json
```

> 📎 N'hésitez pas à lancer les commandes en local !
